var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var cors = require('cors');
require('dotenv/config');


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var jobDescriptionRouter = require('./routes/jobDescriptions');
var applicationsRouter = require('./routes/appliations');
var ccat = require('./routes/ccat');
var quiz = require('./routes/quiz');
let auth = require('./routes/go-improv/auth');
let register = require('./routes/go-improv/register');
let jobPosts = require('./routes/go-improv/jobPosts');
let apply = require('./routes/go-improv/applications');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// db connection
mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true, useUnifiedTopology: true}, () => {
  if (mongoose.connection.readyState !== 3) {
    console.log('MongoDb connected');
    console.log(mongoose.connection.readyState);
    console.log(process.env.DB_CONNECTION);
  } else {
    console.log('MongoDB not Connected');
  }
});

// mongodb+srv://recruitBaeBoo:<password>@recruitapp-mongotest-hayqv.mongodb.net/test

app.use(cors());
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/job-descriptions', jobDescriptionRouter);
app.use('/applications', applicationsRouter);
app.use('/ccats', ccat);
app.use('/quiz', quiz);
app.use('/auth', auth);
app.use('/user', register);
app.use('/jobPosts', jobPosts);
app.use('/application', apply);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
