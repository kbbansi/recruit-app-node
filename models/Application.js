var mongoose = require('mongoose');

const ApplicationSchema = mongoose.Schema({
    userID: Number,
    applicantName: String,
    postID: Number,
    postOwner: Number,
    jobTitle: String,
    totalUserScore: Number,
    averageUserScore: Number,
    userEligibility: String
});

module.exports = mongoose.model('ApplicationSchema', ApplicationSchema);
