var mongoose = require('mongoose');

const KeyPhrase = mongoose.Schema({
    word: String,
    weight: Number
});

module.exports = KeyPhrase;