const mongoose = require('mongoose');

const Questions = mongoose.Schema({
    question: String,
    answer: String,
    answers: [String],
    weight: Number
});

module.exports = Questions;