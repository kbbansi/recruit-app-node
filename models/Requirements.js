var mongoose = require('mongoose');

const Requirement = mongoose.Schema({
    skill: String,
    weight: Number
});

module.exports = Requirement;