var mongoose = require('mongoose');
var requirement = require('./Requirements');
var keyphrase = require('./KeyPhrase');

const JobDescription = mongoose.Schema({
    description: String,
    requirements: [requirement], // requirements is also keyword
    keyphrases: [keyphrase],
    createdBy: Number,
    organization: String,
    title: String,
    postID: Number,
});

module.exports = mongoose.model('JobDescription', JobDescription);