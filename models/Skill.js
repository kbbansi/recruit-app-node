var mongoose = require('mongoose');

const SkillSchema = mongoose.Schema({
    tag: String,
    skillName: String
});


module.exports = SkillSchema;