var mongoose = require('mongoose');
var userSkill = require('./Skill');
var userExperience = require('./Experience');
var duties = require('./Duties');


const UserSchema = mongoose.Schema({
    userID : Number,
    bio: String,
    currentWorkplace: String,
    currentRole: String,
    experience: [userExperience],
    duties: [duties],
    skills: [userSkill]

});

module.exports = mongoose.model('UserSchema', UserSchema);