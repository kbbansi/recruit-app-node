var mongoose = require('mongoose');
var question = require('./Questions');

const CCATQuizSchema = mongoose.Schema({
    title: String,
    createdBy: Number,
    quiz: [question],
});


module.exports = mongoose.model('CCATQuizSchema', CCATQuizSchema);