// CCAT is the model for all psychometric test results

var mongoose = require('mongoose');

const CCATSchema = mongoose.Schema({
    userID: Number,
    applicantName: String,
    jobTitle: String,
    totalUserScore: Number,
    userEligibility: String,
    createdBy: Number,
    totalNumQuestions: Number,
    questionsAnswered: Number,
    totalScore: Number,
    applicationID: String,
    testStatus: String,
    prospectiveStatus: String

});


module.exports = mongoose.model('CCATSchema', CCATSchema);
