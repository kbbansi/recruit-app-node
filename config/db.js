const mysql = require('mysql');

let ConnPool;
// create reusable connection pool

ConnPool = mysql.createPool({
    host: 'mysql-11316-0.cloudclusters.net',
    port: 11316,
    user: 'BaeBoo',
    password: 'recruitBaeBoo',
    database: 'recruit_app_go',
    connectionLimit: 30
});

ConnPool.getConnection( function database(err, connection){
    if (err) {
        switch(err.code) {
            case 'PROTOCOL_CONNECTION_LOST':
                console.log('Database connection was closed');
                break;
    
            case 'ER_CON_COUNT_ERROR':
                console.log('Database has too many connections');
                break;
            
            case 'ECONNREFUSED':
                console.log('Database connection was refused');
                break;
            
            case 'ECONNRESET':
                console.log('The Connection was Reset');
                break;

            case 'ETIMEDOUT':
                console.log('The Connection Got Timed Out');
                console.log(err.code);
                break;
            default:
                console.log(err.code);
        }
    }

    if (connection) {
        console.log('Connection Established');
        connection.release();
    }
});


module.exports = ConnPool;
