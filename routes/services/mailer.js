/*
mailer will be the email sending service for verifying 
an organization
mailer will use the NodeMailer package to send emails in conjuntion with
Goolge's GMAIL API :)
ClientID: 747563797159-2vt677rriga8ahn80mqhef0ceol0iebf.apps.googleusercontent.com
clientSecret: dmO_cITjwf_CEKqb5SIXnk8u
redirect uri: https://developers.google.com/oauthplayground
refresh token: 1//04JMxyo5KlNt9CgYIARAAGAQSNwF-L9Ir2aE4_94RYWfbxN6MVU8gwBt91DU34EEBKFQWcc5MBJkLr2gj5y5U1KlLenrKAb_5aPc
access token: ya29.a0AfH6SMC7EzHvdJLnjC-A3-zbeKC0vgSN6FlayRDly6sqk1lkjlreribZ5I2N4yg5XnCcZ6FOyOHTVjofhtbHFmtNXHkr9TFMeN5IucQej1KJISDZU8fl4gpm6FxwTlMmTitL75l6Lgpw7W3MPWfR0jZ3FwZumsq8Cqc
*/

const nodeMailer = require('nodemailer');
const { google } = require('googleapis');
const oAuth2 = google.auth.OAuth2;

// oAuth2Client sets the oAuth2 credentials
const oAuth2Client = new oAuth2(
    "747563797159-2vt677rriga8ahn80mqhef0ceol0iebf.apps.googleusercontent.com",
    "dmO_cITjwf_CEKqb5SIXnk8u",
    "https://developers.google.com/oauthplayground"
);

// set up refresh token
oAuth2Client.setCredentials({
    refresh_token: "1//04JMxyo5KlNt9CgYIARAAGAQSNwF-L9Ir2aE4_94RYWfbxN6MVU8gwBt91DU34EEBKFQWcc5MBJkLr2gj5y5U1KlLenrKAb_5aPc"
});

// get an access token to be used in authenticating with the GMAIL API
const newAccessToken = oAuth2Client.getAccessToken();

// global variable for handling creating mail object
let transport, mailObject, mailOptions;

// the mail function take a service name and associated data to send emails
exports.mail = function (serviceName, data) {
    switch (serviceName) {
        // case organization will handle sending emails to user's who register as an organization
        case 'register':
            // transport holds an instance of a nodeMailer mail object
            transport = nodeMailer.createTransport({
                service: 'gmail', // set mail service to gmail
                
                // set authentication values/parameters
                auth: {
                    type: "OAuth2",
                    clientId: "747563797159-2vt677rriga8ahn80mqhef0ceol0iebf.apps.googleusercontent.com",
                    clientSecret: "dmO_cITjwf_CEKqb5SIXnk8u",
                    access_Type: "offline"
                }
            });
            // check for callback event and log out some information
            transport.on('token', token => {
                console.log('Got a new Access token');
                console.log('User: %s', token.user);
                console.log('Access Token: %s', token.accessToken);
                console.log('Expires in: %s', new Date(token.expires));
            });
            // create email object
            mailObject = {
                to: data.to,
                from: data.from,
                subject: data.subject,
                message: 'Hello' + data.first_name + '\nYou have registered on the RecruitApp Platform.\nClick the link below to login\n\n',
                htmlMessage: `<strong>Hello ${data.first_name}</strong>
                            <p>You have registered on the RecruitApp Platform.</p>
                            <p>Click the link below to login.</p>
                `
            };
            // parameters for sending email
            mailOptions = {
                from: mailObject.from,
                to: mailObject.to,
                subject: 'Registration Success',
                text: mailObject.message,
                html: mailObject.htmlMessage,
                auth: {
                    user: "kwabenaampofo5@gmail.com",
                    refreshToken: "1//04JMxyo5KlNt9CgYIARAAGAQSNwF-L9Ir2aE4_94RYWfbxN6MVU8gwBt91DU34EEBKFQWcc5MBJkLr2gj5y5U1KlLenrKAb_5aPc",
                    accessToken: newAccessToken,
                    expires: 1484314697598
                },
            };
            // send mail
            transport.sendMail(mailOptions, function (err, info) {
                if (err) {
                    console.log(err.message.toString());
                    return err;
                } else {
                    console.log(info);
                    return 200;
                }
            });
            break;

        case 'application':
            transport = nodeMailer.createTransport({
                service: 'gmail',
                auth: {
                    type: "OAuth2",
                    clientId: "747563797159-2vt677rriga8ahn80mqhef0ceol0iebf.apps.googleusercontent.com",
                    clientSecret: "dmO_cITjwf_CEKqb5SIXnk8u",
                    access_Type: "offline"
                }
            });
            transport.on('token', token => {
                console.log('Got a new Access token');
                console.log('User: %s', token.user);
                console.log('Access Token: %s', token.accessToken);
                console.log('Expires in: %s', new Date(token.expires));
            });
            mailObject = {
                to: data.to,
                from: data.from,
                subject: data.subject,
                message: 'Almost there',
                htmlMessage: `<strong>Almost there</strong>`
            };
            mailOptions = {
                from: mailObject.from,
                to: mailObject.to,
                subject: mailObject.subject,
                text: mailObject.message,
                html: mailObject.htmlMessage,
                auth: {
                    user: "kwabenaampofo5@gmail.com",
                    refreshToken: "1//04JMxyo5KlNt9CgYIARAAGAQSNwF-L9Ir2aE4_94RYWfbxN6MVU8gwBt91DU34EEBKFQWcc5MBJkLr2gj5y5U1KlLenrKAb_5aPc",
                    accessToken: newAccessToken,
                    expires: 1484314697598
                },
            };
            transport.sendMail(mailOptions, function (err, info) {
                if (err) {
                    console.log(err.message.toString());
                    return err;
                } else {
                    console.log(info);
                    return 200;
                }
            });
            break;

            default:
                var defaultService = { status: 503, message: 'Forbbiden'};
                return defaultService;
    }
};
