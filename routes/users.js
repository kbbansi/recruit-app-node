const express = require('express');
const router = express.Router();
const user = require('../models/User');
const mongoose = require('mongoose');


/* GET user profile */
router.get('/', async function (req, res, err) {
  console.log(mongoose.connection.readyState);
  try {
    const profiles = await user.find();
    console.log(profiles[0].id);
    res.status(200).json({
      status: 200,
      message: profiles
    });
  } catch (err) {
    console.log(`An Error Occurred: ${err}`);
    res.status(503).json({
      status: 400,
      message: `An Error Occurred: ${err}`
    });
  }
});

// Get Specific Profile
router.get('/:userID', async function (req, res) {
  let userID = req.params.userID;
  try {
    const profile = await user.find({ "userID": userID });
    console.log(profile.experience);
    console.log(profile.skills);
    res.status(200).json({
      status: 200,
      message: profile
    });
  } catch (err) {
    console.log(`An Error Occurred: ${err}`);
    res.status(400).json({
      status: 400,
      message: err.message
    });
  }
});

// Create UserProfile
router.post('/add_profile', async function (req, res) {

  // a test:: passed
  // res.status(200);
  // res.json({
  //   status: 200,
  //   stream: req.body
  // });

  const UserProfile = new user({
    userID: req.body.userID,
    bio: req.body.bio,
    currentWorkplace: req.body.currentWorkplace,
    currentRole: req.body.currentRole,
    experience: req.body.experience,
    duties: req.body.duties,
    skills: req.body.skills
  });

  // refactor to use async and await
  try {
    const response = await UserProfile.save();
    res.status(200).json({
      status: 200,
      message: response.id,
      userStream: UserProfile,
      responseBody: response
    });
  } catch (err) {
    console.log(`Error: ${err}`);
    res.status(400).json({
      status: 400,
      message: err
    });
  }
  // UserProfile.save().then(queryResponse => {
  //   console.log(UserProfile);
  //   res.status(200).json(queryResponse);
  // }).catch(err => {
  //   console.log(`Bad Request: ${err}`);
  //   res.status(400).json(err);
  // });
});


module.exports = router;
