var express = require('express');
var router = express.Router();
var jobDescription = require('../models/JobDescription');
var isEmpty = require('is-empty');

var postID;
// Get all job descriptions
router.get('/', async function (req, res) {
    try {
        const jobDescriptions = await jobDescription.find();
        if (isEmpty(jobDescriptions)) {
            console.log('No Job Descriptions Created Yet');
            res.status(404).json({
                status: 404,
                message: `No Job Descriptions Created Yet`
            });
        } else {
            console.log(jobDescriptions);
            res.status(200).json({
                status: 200,
                message: jobDescriptions
            });
        }
    } catch (err) {
        console.log(`An Error Occurred: ${err}`);
        res.status(400).json({
            status: 400,
            message: err.message
        });
    }
});

//todo:: Get Specific Job Description
router.get('/:postID', async function (req, res) {
    postID = req.params.postID;
    try {
        const job = await jobDescription.find({ "postID": postID });
        if (isEmpty(job)) {
            console.log('No Matching Job Description with ID:', documentID);
            res.status(404).json({
                status: 404,
                message: `No Matching Job Description with ID: ${documentID}`
            });
        } else {
            console.log(job);
            res.status(200).json({
                status: 200,
                message: job
            });
        }
    } catch (err) {
        console.log(`An Error Occurred: ${err}`);
        res.status(400).json({
            status: 400,
            message: err.message
        });
    }
});


//todo:: Get Job Descriptions Created by a particular user
router.get('/job-descriptions/:createdBy', async function (req, res) {
    var createdBy = req.params.createdBy;
    try {
        const job = await jobDescription.find({ "createdBy": createdBy });
        if (isEmpty(job)) {
            console.log('No Matching Job Description created by user:', createdBy);
            res.status(404).json({
                status: 404,
                message: `No Matching Job Description created by user: ${createdBy}`
            });
        } else {
            console.log(job);
            res.status(200).json({
                status: 200,
                message: job
            });
        }
    } catch (err) {
        console.log(`An Error Occurred: ${err}`);
        res.status(400).json({
            status: 400,
            message: err.message
        });
    }
});


//todo:: Create Job Description
router.post('/add_jobDescription', async function (req, res) {
    const Description = new jobDescription({
        description: req.body.description,
        requirements: req.body.requirements,
        keyphrases: req.body.keyphrases,
        createdBy: req.body.createdBy,
        organization: req.body.organization,
        title: req.body.title,
        postID: req.body.postID
    });

    try {
        const response = await Description.save();
        res.status(201).json({
            status: 201,
            message: response.id,
            responseBody: response,
            action: 'JOB DESCRIPTION ADDED',
        });
    } catch (err) {
        console.log(`An Error Occurred: ${err}`);
        res.status(400).json({
            status: 400,
            message: err.message
        });
    }
});


//todo:: Update Job Description

//todo:: Delete Job Description


module.exports = router;