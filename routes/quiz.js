const express = require('express');
const router = express.Router();
const Quiz = require('../models/Quiz');


router.post('/create-quiz', async function (req, res) {
   try {
       const quiz = new Quiz({
           quiz: req.body.quiz,
           title: req.body.title,
           createdBy: req.body.createdBy
       });

       const response = await quiz.save();
       res.status(201).json({
           status: 201,
           message: response,
           stream: quiz
       });
   } catch (error) {
       console.log(error);
       res.status(400).json({
           status: 400,
           message: error
       });
   }
});


router.get('/get-questions', async function (req, res) {
    let questions=[], data, answers=[], correct=[], weight =[];
    try {
        const quizzes = await Quiz.find();
        console.log(quizzes[0].title);
        res.status(200).json({
            status: 200,
            message: quizzes
        });
    } catch (error) {
        console.log(error);
       res.status(400).json({
           status: 400,
           message: error
       });
    }
});

router.post('/score-quiz', async function (req, res) {
    // quiz processing
    try {
        
    } catch (error) {
        
    }
})

module.exports = router;