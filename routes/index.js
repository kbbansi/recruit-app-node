var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.status(200);
  res.json({
    status: 200,
    service: 'RecruitApp Node-Mongo',
    date: new Date()
  });
});

module.exports = router;
