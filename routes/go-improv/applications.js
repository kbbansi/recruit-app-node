const express = require('express');
const router = express.Router();
const isEmpty = require('is-empty');
let con_pool = require('../../config/db');

let application, query, id;

// create will create an application on the mysql server
// user will be asked to complete application process where
// user profile will be scored and ranked on the node server,
router.post('/create', function (req, res, err) {
    if (req) {
        // build request object
        application = {
            userID: req.body.userID,
            postID: req.body.postID,
            title: req.body.title,
            applicantName: req.body.applicantName,
            job_post_document_id: req.body.job_post_document_id
        };
        // check for empty values
        if (isEmpty(application.userID) || isEmpty(application.postID) || isEmpty(application.title) ||
            isEmpty(application.job_post_document_id)) {
            console.log(application);
            console.log('Missing field-value: %s', application);
            res.status(400);
            res.json({
                status: 400,
                reason: 'Empty Request Body'
            });
        } else {
            query = `Insert into applications set?`;
            con_pool.query(query, application, function (err, rows) {
                if (!err) {
                    res.status(201);
                    res.json({
                        status: 201,
                        data: {
                            id: rows.insertId,
                            action_type: 'Application Created'
                        }
                    });
                } else {
                    console.log(err);
                    res.status(400);
                    res.json({
                        status: 400,
                        reason: 'Unable to create resource at this time'
                    });
                }
            })
        }
    } else {
        console.log(err);
        res.status(500);
        res.json({
            status: 500,
            reason: 'Internal Server Error' + err.message
        });
    }
});

router.get('/user/:id', function (req, res) {
    // get all applications made by a user
    id = req.params.id;

    // convert id to int
    let userID = parseInt(id, 10);
    console.log(userID);
    console.log(typeof userID);

    query = `Select * from applications where userID = ${userID}`;
    con_pool.query(query, function (err, rows) {
        if (!err) {
            console.log(query);
            res.status(200);
            res.json({
                status: 200,
                data: rows
            });
        } else {
            console.log(err);
            res.status(404);
            res.json({
                status: 404,
                reason: 'Could Not Get list of User Applications'
            });
        }
    });
});

module.exports = router;
