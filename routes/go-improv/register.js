const express = require('express');
const router = express.Router();
const isEmpty = require('is-empty');
const hashAlg = require('../../config/password');
let con_pool = require('../../config/db');
let mailService = require('../services/mailer');

let user, query;

// get all users
router.get('/', function (req, res) {
    if (req) {
        query = `Select * from users`;
        con_pool.query(query, function (err, rows) {
            if (isEmpty(rows)) {
                console.log('No Users');
                res.status(404);
                res.json({
                    status: 404,
                    reason: 'No Users Found'
                });
            } else {
                console.log(query, rows[0]);
                res.status(200);
                res.json({
                    status: 200,
                    data: rows
                });
            }
        });
    } else {
        console.log('An Error Occurred');
        res.status(400);
        res.json({
            status: 400,
            reason: 'Could not complete request at the given time'
        });
    }
});

// get one user
router.get('/:id', function (req, res, err) {
    if (req) {
        let userID = req.params.id;
        let id = parseInt(userID, 10);

        console.log(id);
        console.log(typeof id);

        query = `Select * from users where id = ${id}`;
        con_pool.query(query, function (err, rows) {
            if (!err) {
                if (isEmpty(rows)){
                    console.log('Empty Row set, No user found');
                    res.status(404);
                    res.json({
                        status: 404,
                        reason: 'Empty Row set, No user found'
                    });
                } else {
                    console.log(query, rows);
                    res.status(200);
                    res.json({
                        status: 200,
                        data: rows
                    });
                }
            } else {
                console.log(err.sqlMessage);
                res.status(400);
                res.json({
                    status: 400,
                    code: err.code,
                    reason: 'Bad Request: ' + err.sqlMessage
                });
            }
        });
    } else {
        console.log('An Error Occurred', err);
        res.status(500);
        res.json({
            status: 500,
            reason: 'Internal Server Error' + err
        });
    }
});

// register will create a new user onto the system
router.post('/register', function (req, res, err) {
    if (req) {
        user = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            other_names: req.body.other_names,
            contact: req.body.contact,
            email: req.body.email,
            password: hashAlg.passwordHash(req.body.password),
            created_on: new Date(),
        };

        // check for empty fields
        if (isEmpty(user.first_name) || isEmpty(user.last_name) || isEmpty(user.contact)  || isEmpty(user.email) || isEmpty(user.password)) {
            console.log('No Data Supplied');
            res.status(400);
            res.json({
                status: 400,
                reason: 'No Data Supplied',
            });
        } else {
            // todo ::: verify user email
            // query = `Insert into users (first_name, last_name, other_names,
            //        contact, email, password,
            //        created_on)
            //        VALUES ('${user.first_name}', '${user.last_name}', '${user.other_names}', '${user.contact}', '${user.email}',  '${user.password}',  '${user.created_on}');`;
            query = `Insert into users set?`;
            con_pool.query(query, user,function (err, rows) {
                if (!err) {
                    res.status(200);
                    res.json({
                        status: 201,
                        data: {
                            id: rows.insertId,
                            action_type: 'User Created'
                        }
                    });
                    mailData = {
                        to: req.body.email,
                        from: 'kwabenaampofo5@gmail.com',
                        first_name: req.body.first_name
                    };

                    mailService.mail('register', mailData);
                } else {
                    console.log(err.message);
                    res.status(400);
                    res.json({
                        status: 400,
                        reason: 'Unable to create Resource'
                    });
                }
            });

        }
    } else {
        console.log(err);
        res.status(500);
        res.json({
            status: 500,
            reason: 'An Unhandled Error'
        });
    }
});

router.put('/update/:id', function (req, res, err) {
    if (req) {
        let id = parseInt(req.body.id, 10);
        user = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            other_names: req.body.other_names,
            contact: req.body.contact,
            email: req.body.email,
            user_type: req.body.user_type,
            modified_on: new Date(),
        };

        // check for empty fields
        if (isEmpty(user.first_name) || isEmpty(user.last_name) || isEmpty(user.contact)  || isEmpty(user.email)) {
            console.log('No Data Supplied');
            res.status(400);
            res.json({
                status: 400,
                reason: 'No Data Supplied',
            });
        } else {
            // todo ::: verify user email
            // query = `Insert into users (first_name, last_name, other_names,
            //        contact, email, password,
            //        created_on)
            //        VALUES ('${user.first_name}', '${user.last_name}', '${user.other_names}', '${user.contact}', '${user.email}',  '${user.password}',  '${user.created_on}');`;
            query = `Update users Set? where id = ${id}`;
            con_pool.query(query, user,function (err, rows) {
                if (!err) {
                    res.status(200);
                    res.json({
                        status: 200,
                        data: {
                            id: rows.insertId,
                            action_type: 'User Updated'
                        }
                    });
                } else {
                    console.log(err.message);
                    res.status(400);
                    res.json({
                        status: 400,
                        reason: 'Unable to create Resource'
                    });
                }
            });

        }
    } else {
        console.log(err);
        res.status(500);
        res.json({
            status: 500,
            reason: 'An Unhandled Error'
        });
    }
});

module.exports = router;
