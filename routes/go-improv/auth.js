const express = require('express');
const router = express.Router();
const isEmpty = require('is-empty');
const hashAlg = require('../../config/password');
let con_pool = require('../../config/db');

let auth, passwordCheckQuery, query, encryptedPassword;
// auth route will handle authentication of users
router.post('/login', function (req, res, err) {
    auth = {
        email: req.body.email,
        password: req.body.password
    };

    passwordCheckQuery = `Select password from users where email = '${auth.email}'`;
    con_pool.query(passwordCheckQuery, function (err, rows) {
        if (!err) {
            if (isEmpty(rows)) {
                console.log(`SQL returned empty row set\n User ${auth.email} does not exisit`);
                res.status(404);
                res.json({
                    status: 404,
                    reason: `SQL returned empty row set\r User ${auth.email} does not exisit`
                });
            } else {
                // set status to ok to prevent crash
                res.status(200);

                // store sql result into variable
                encryptedPassword = rows[0];

                //hashMatch is the bool result for the hashCompare method
                let hashMatch = hashAlg.hashCompare(auth.password, encryptedPassword.password);
                console.log(hashMatch);

                //if hashMatch === true
                if (hashMatch) {
                    query = `Select * from users where email = '${auth.email}'`;
                    con_pool.query(query, function (err, rows) {
                        if (!err) {
                            console.log(query);
                            console.log(rows[0]);
                            res.status(200);
                            res.json({
                                status: 200,
                                message: 'User Logged In',
                                session_data: rows[0]
                            });
                        } else {
                            console.log(err);
                            res.status(400);
                            res.json({
                                status: 400,
                                reason: 'FAIL, BAD REQUEST: ' + err
                            });
                        }
                    });
                } else {
                    console.log('Supplied Password and Hash don\'t match');
                    res.status(400);
                    res.json({
                        status: 400,
                        reason: 'Supplied Password and Hash do not Match'
                    });
                }

            }
        } else {
            res.status(500);
            res.json({
                status: 500,
                reason: 'Internal Server Error' + err
            })
        }
    });
});




module.exports = router;