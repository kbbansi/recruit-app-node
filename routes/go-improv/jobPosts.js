// jobPosts is the handler for all job posts on the system
const express = require('express');
const router = express.Router();
const isEmpty = require('is-empty');
let con_pool = require('../../config/db');

let jobPost, query;
// get all job posts
router.get('/', function (req, res) {
    query = `Select * from job_posts;`;
    con_pool.query(query, function (err, rows) {
        if (!err) {
            if (isEmpty(rows)) {
                console.log('An Error Occurred');
                res.status(404);
                res.json({
                    status: 404,
                    reason: 'Could Not Get List of Job Posts'
                });
            } else {
                console.log(rows);
                res.status(200);
                res.json({
                    status: 200,
                    data: rows
                });
            }
        } else {
            console.log(err);
            res.status(400);
            res.json({
                status: 400,
                reason: 'Bad Request' + err.message
            });
        }
    });
});

// get all user job posts
router.get('/:id', function (req, res) {
    let id = req.params.id;

    // parse int to convert strings to numbers
    let created_by = parseInt(id, 10);
    console.log(created_by);
    console.log(typeof created_by);

    // res.status(200);
    // res.json({
    // .sqlM    status: 200,
    //     created_by: created_by
    // });
    query = `Select * from job_posts where created_by = ${created_by}`;
    con_pool.query(query, function (err, rows) {
        if (!err) {
            if (isEmpty(rows)) {
                console.log('User has no Jobs');
                res.status(404);
                res.json({
                    status: 404,
                    reason: 'User has no Jobs'
                });
            } else {
                console.log(query, rows);
                res.status(200);
                res.json({
                    status: 200,
                    data: rows
                });
            }
        } else {
            console.log(err.sqlMessage);
            res.status(400);
            res.json({
                status: 400,
                code: err.code,
                reason: 'Bad Request: ' + err.sqlMessage
            });
        }
    });
});

// add a new job post
router.post('/create', function (req, res, err) {
    if (req) {
        jobPost = {
            title: req.body.title,
            organization: req.body.organization,
            created_by: req.body.created_by
        };

        if (isEmpty(jobPost.title) || isEmpty(jobPost.organization) || isEmpty(jobPost.created_by)) {
            console.log('Payload is Empty');
            res.status(400);
            res.json({
                status: 400,
                reason: 'Payload is Empty'
            });
        } else {
            query = `Insert into job_posts set?`;
            con_pool.query(query, jobPost, function (err, rows) {
                if (!err) {
                    console.log(query, jobPost);
                    res.status(201);
                    res.json({
                        status: 201,
                        data: {
                            id: rows.insertId,
                            action_type: 'Job Post Created'
                        }
                    });
                }
            });
        }
    } else {
        console.log('An Error Occurred: ', err)
        res.status(500);
        res.json({
            status: 500,
            reason: 'Internal Server Error'
        });
    }
});

module.exports = router;
