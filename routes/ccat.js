const express = require('express');
const router = express.Router();
const UserProfile = require('../models/User');
const CCAT = require('../models/CCAT');
const isEmpty = require('is-empty');

// initialze a u ccat test session
router.post('/init-ccat', async function (req, res) {
    let userID = req.body.userID;
    let createdBy = req.body.createdBy;
    let applicationID = req.body.applicationID;
    let userEligibility = req.body.userEligibility;
    let applicantName = req.body.applicantName;
    let jobTitle = req.body.jobTitle;
    let totalUserScore = req.body.totalUserScore;

    try {
        const ccat = new CCAT({
            userID: userID,
            applicantName: applicantName,
            jobTitle: jobTitle,
            createdBy: createdBy,
            applicationID: applicationID,
            totalUserScore: totalUserScore,
            userEligibility: userEligibility
        });

        const response = await ccat.save();
        res.status(201).json({
            status: 201,
            message: response.id,
            ccatStream: ccat,
            responseBody: response
        });

    } catch (error) {
        console.log(`Error: ${error}`);
        res.status(400).json({
            status: 400,
            message: error
        });
    }
});

router.get('/get-all-ccat-candidates', async function (req, res) {
    try {
        const ccat = await CCAT.find();
        console.log(ccat);
        if (isEmpty(ccat)) {
            res.status(404).json({
                status: 404,
                message: 'No CCAT Candidates found'
            });
        } else {
            res.status(200).json({
                status: 200,
                message: ccat
            });
        }
    } catch (error) {
        console.log(`Error: ${error}`);
        res.status(400).json({
            status: 400,
            message: error
        });
    }
});

router.get('/get-user-ccat/:userID', async function (req, res) {
    const userID = req.params.userID;
    try {
        const userCCAT = await CCAT.find({userID: userID});
        console.log(userCCAT);

        if (isEmpty(userCCAT)) {
            res.status(404).json({
                status: 404,
                message: 'No CCAT Candidates found'
            });
        } else {
            res.status(200).json({
                status: 200,
                message: userCCAT
            });
        }
    } catch (error) {
        console.log(`81Error: ${error}`);
        res.status(400).json({
            status: 400,
            message: error
        });
    }
});

router.put('/take-ccat', async function (req, res) {
    const testStatus = req.body.testStatus;

    try {
        const ccat = await CCAT.findByIdAndUpdate({ _id: req.body._id }, { testStatus: testStatus });
        console.log(ccat);
        res.status(202).json({
            status: 202,
            message: ccat
        });
    } catch (error) {
        console.log(`Error: ${error}`);
        res.status(400).json({
            status: 400,
            message: error
        });
    }

});

module.exports = router;
