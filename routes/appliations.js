const express = require('express');
const router = express.Router();
const UserProfile = require('../models/User');
const Job = require('../models/JobDescription');
const Applications = require('../models/Application');
const isEmpty = require('is-empty');

router.get('/', async function (req, res) {
    try {
        res.status(200);
        await res.json({
            route: "Applications",
            service: "Recruit APP Node Service",
            date: new Date()
        });
    } catch (err) {
        console.log(err.message);
        res.status(400).json({
            status: 400,
            message: err.message
        });
    }
});


router.post('/apply', async function (req, res) {
    // get userID, and job post ID
    let userID = req.body.userID;
    let applicantName = req.body.applicantName;
    let postID = req.body.postID;
    let jobTitle, createdBy;

    try {
        const user = await UserProfile.findOne({ 'userID': userID });
        const job = await Job.findOne({ 'postID': postID });
        let skillRequirementScore = 0;
        let dutyPhraseScore = 0;

        let skills = user.skills;
        let userDuties = user.duties;

        jobTitle = job.title;
        createdBy = job.createdBy;
        let requirement_token = job.requirements;
        let keyphrases = job.keyphrases;

        console.log(requirement_token);
        console.log("===================================================");
        console.log(skills);
        console.log("===================================================");
        console.log(keyphrases);
        console.log("===================================================");
        console.log(userDuties);
        console.log("===================================================");

        res.status(200);

        // loop through user skills
        console.log(skills.length);
        console.log('Job Requirement\t\t\tUser Skill');
        for (let i = 0; i < requirement_token.length; i++) {
            // get skill name of user skill
            // inner for loop will get requirements and check if against user skill
            for (let j = 0; j < skills.length; j++) {
                console.log(requirement_token[i].skill, '\t', skills[j].skillName, '\t', skills[j].tag);
                if (requirement_token[i].skill.includes(skills[j].skillName) || requirement_token[i].skill.includes(skills[j].tag)) {
                    skillRequirementScore += requirement_token[i].weight;
                } else {
                    console.log('Uh-oh');
                }
            }
        }

        for (let a = 0; a < keyphrases.length; a++) {
            for (let b = 0; b < skills.length; b++) {
                console.log(keyphrases[a].word, '\t\t', skills[b].tag, '\t', skills[b].skillName);
                if (keyphrases[a].word.includes(skills[b].tag) || keyphrases[a].word.includes(skills[b].skillName)) {
                    dutyPhraseScore += keyphrases[a].weight;
                } else {
                    console.log('Uh oh');
                }
            }
        }
        console.log(skillRequirementScore);
        console.log(dutyPhraseScore);

        let totalScore = skillRequirementScore + dutyPhraseScore;
        let avgScore = totalScore / 2;

        if (skillRequirementScore > 150) {
            const application = new Applications({
                userID: userID,
                applicantName: applicantName,
                postID: postID,
                jobTitle: jobTitle,
                postOwner: createdBy,
                totalUserScore: totalScore,
                averageUserScore: avgScore,
                userEligibility: 'Eligible'
            });

            try {
                const response = await application.save();
                res.status(201).json({
                    status: 201,
                    message: response.id,
                    applicationStream: application,
                    responseBody: response
                });
            } catch (error) {
                console.log(`Error: ${error}`);
                res.status(400).json({
                    status: 400,
                    message: error
                });
            }
        } else {
            const application = new Applications({
                userID: userID,
                applicantName: applicantName,
                postID: postID,
                jobTitle: jobTitle,
                postOwner: createdBy,
                totalUserScore: totalScore,
                averageUserScore: avgScore,
                userEligibility: 'Not Eligible'
            });
            try {
                const response = await application.save();
                res.status(201).json({
                    status: 201,
                    message: response.id,
                    applicationStream: application,
                    responseBody: response
                });
            } catch (error) {
                console.log(`Error: ${error}`);
                res.status(400).json({
                    status: 400,
                    message: error
                });
            }
        }

    } catch (error) {
        console.log(`Error: ${error}`);
        res.status(400).json({
            status: 400,
            message: error
        });
    }

});


router.get('/applications-made/:userID', async function (req, res) {
    // applications-made will get 1. All job applications a user has made
    let userID = req.params.userID;

    try {
        const user = await Applications.find({ 'userID': userID });

        if (isEmpty(user)) {
            console.log('No Application found for user:', userID);
            res.status(404).json({
                status: 404,
                message: `No Application found for user: ${userID}`
            });
        } else {
            console.log('Applicant Name: ', user[0].applicantName);
            for (let i = 0; i < user.length; i++) {
                console.log('Eligibility Status: ',user[i].userEligibility);
                console.log('Total Score: ',user[i].totalUserScore);
                console.log('Job Title: ',user[i].jobTitle);
            }
            res.status(200).json({
                status: 200,
                message: user
            });
        }

    } catch (error) {
        console.log(`Error: ${error}`);
        res.status(400).json({
            status: 400,
            message: error
        });
    }

});

router.get('/applications-received/:createdBy', async function (req, res) {
    let postOwner = req.params.postOwner;
    try {
        const user = await Applications.find({ 'createdBy': postOwner });

        if (isEmpty(user)) {
            console.log('No Application Made to Job Posts by user:', postOwner);
            res.status(404).json({
                status: 404,
                message: `No Application Made to Job Posts by user: ${postOwner}`
            });
        } else {
            console.log('Applicant Name: ', user[0].applicantName);
            for (let i = 0; i < user.length; i++) {
                console.log('Eligibility Status: ',user[i].userEligibility);
                console.log('Total Score: ',user[i].totalUserScore);
                console.log('Job Title: ',user[i].jobTitle);
            }
            res.status(200).json({
                status: 200,
                message: user
            });
        }

    } catch (error) {
        console.log(`Error: ${error}`);
        res.status(400).json({
            status: 400,
            message: error
        });
    }

});

module.exports = router;
